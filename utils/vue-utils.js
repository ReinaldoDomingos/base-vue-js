function gerarDataListOptions(item, atributo) {
    return {id: item.id, label: item[atributo]};
}

function gerarListaPaginadaVazia() {
    return {content: [], number: 0, size: 5, numberOfElements: 0, totalElements: 0};
}